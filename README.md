# The Critter Package

## Introduction

This package is for iterative image reconstruction via a variety of methods. Right now it just supports SCR and STCR (via TV regularization) but in the future it should support other methods such as ESPIRiT with L1-Wavelets, multi-coil STCR and so on.

### Version

The VERSION.mat file contains a single variable `VERSION` which contains a string with the version number of this package. No modifications to this file should be made without updating this number according to [semantic versioning best practices](http://semver.org/).

### Package Management

Running `Critter.verify` will examine the environment that Critter sees and ensure that any packages it depends on are available and of the correct version.

### Tests

To make sure this software works as expected in the present environment, tests have been provided and can be run by executing `Critter.test`.

### Docs

In the `docs/` directory some markdown files are provided explaining how to use each of the functions and classes this package provides.
