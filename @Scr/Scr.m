classdef Scr < handle
  properties (Constant)
    DEBUG = false;
    BETA_SQUARED = 1e-8;
    STEP_SIZE = 0.5;
    N_ITERATIONS = 70;
    MAGIC_SCALE_NUMBER = 4;
  end
  properties
    kSpaceInput
    fftObject
    Weights
    imageInput
    nIterations

    scaleFactor
    imageEstimate
    maskedImageEstimate
    fidelityUpdateTerm
    spatialUpdateTerm
  end
  methods
    function self = Scr(kSpaceInput, fftObj, Opts)
      self.kSpaceInput = single(kSpaceInput);
      self.fftObject = fftObj;
      self.Weights = Opts.Weights;
      % TODO: Make this dynamic given defaults from constants
      if isfield(Opts, 'nIterations')
        self.nIterations = Opts.nIterations;
      else
        self.nIterations = self.N_ITERATIONS;
      end
    end

    function finalImage = reconstruct(self)
      self.create_image_input();
      self.scale_image_input();
      self.iteratively_reconstruct();
      finalImage = self.unscale_image();
    end

    function create_image_input(self)
      self.imageInput = self.fftObject' * self.kSpaceInput;
    end

    function scale_image_input(self)
      maxIntensity = max(abs(self.imageInput(:)));
      self.scaleFactor = self.MAGIC_SCALE_NUMBER / maxIntensity;
      self.imageInput = single(self.imageInput * self.scaleFactor);
    end

    function imageEstimate = iteratively_reconstruct(self)
      isStcr = isa(self, 'Critter.Stcr');
      self.pre_allocate_loop_variables();
      for iIteration = 1:self.nIterations
        self.update_fidelity_term();
        self.update_spatial_term();
        if isStcr
          self.update_temporal_term();
        end
        self.update_image_estimate();
        self.update_masked_image_estimate();
      end
    end

    function pre_allocate_loop_variables(self)
      self.imageEstimate = self.imageInput;
      self.maskedImageEstimate = self.imageEstimate;
    end

    function update_fidelity_term(self)
      fidelityTerm = self.imageInput - self.maskedImageEstimate;
      self.fidelityUpdateTerm = self.Weights.fidelity * fidelityTerm;
    end

    function update_spatial_term(self)
      spatialTerm = TV_Spatial_2D(self.imageEstimate, self.BETA_SQUARED);
      self.spatialUpdateTerm = self.Weights.spatial * spatialTerm;
    end

    function update_image_estimate(self)
      imageUpdate = self.STEP_SIZE * ...
                        (self.fidelityUpdateTerm + self.spatialUpdateTerm);
      self.imageEstimate = self.imageEstimate + imageUpdate;
    end

    function update_masked_image_estimate(self)
      kSpaceEstimate = self.fftObject * self.imageEstimate;
      self.maskedImageEstimate = self.fftObject' * kSpaceEstimate;
    end

    function finalImage = unscale_image(self)
      finalImage = self.imageEstimate ./ self.scaleFactor;
    end
  end
end
