classdef Stcr < Critter.Scr
  properties
    temporalUpdateTerm
  end
  methods
    function self = Stcr(kSpaceInput, fftObj, Opts)
      self@Critter.Scr(kSpaceInput, fftObj, Opts)
    end

    function pre_allocate_loop_variables(self)
      % Call super
      pre_allocate_loop_variables@Critter.Scr(self);
      % Add to it the temporal term
      self.temporalUpdateTerm = zeros(size(self.imageEstimate), 'single');
    end

    function update_image_estimate(self)
      % Call super
      update_image_estimate@Critter.Scr(self);
      % Add temporal term
      imageUpdate = self.STEP_SIZE * self.temporalUpdateTerm;
      self.imageEstimate = self.imageEstimate + imageUpdate;
    end

    function update_temporal_term(self)
      % Take two differences in time dimension
      firstDiff = diff(self.imageEstimate, 1, 3);
      normalizationFactorSquared =  self.BETA_SQUARED + abs(firstDiff).^2;
      firstDiffNorm = firstDiff ./ sqrt(normalizationFactorSquared);
      secondDiff = diff(firstDiffNorm, 1, 3);

      % Fill term with pieces of differences
      self.temporalUpdateTerm(:,:,1) = firstDiffNorm(:,:,1);
      self.temporalUpdateTerm(:,:,2:end-1) = secondDiff;
      self.temporalUpdateTerm(:,:,end) = -firstDiffNorm(:,:,end);

      % Save as weighted temporal update term
      self.temporalUpdateTerm = self.Weights.temporal * self.temporalUpdateTerm;
    end

  end
end
